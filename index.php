<?php
require "bootstrap.php";
use Angular2018\Models\User;
use Angular2018\Models\Product;


$app = new \Slim\App();

$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});

$app->get('/products', function($request, $response,$args){
    $_product = new Product();
    $products = $_product->all(); //מערך של אוביקטים 
    $payload = [];
    foreach($products as $pdt){
        $payload[$pdt->id] = [
            'name'=>$pdt->name,
            'price'=>$pdt->price,
        ];        
    }
    return $response->withStatus(200)->withJson($payload);
 });

 $app->get('/products/{id}', function($request, $response,$args){
   $_id = $args['id']; 
   $product = Product::find($_id); 
   return $response->withStatus(200)->withJson($product);
});

$app->put('/products/{product_id}', function($request, $response, $args){
    $name = $request->getParsedBodyParam('name', '');
	$price = $request->getParsedBodyParam('price', '');
    $_product = Product::find($args['product_id']);
    $_product->name = $name;
	$_product->price = $price;
    if($_product->save()){
        $payload = ['product_id'=>$_product->id, "result"=>"The product has been updated successfuly"];
        return $response->withStatus(200)->withJson($payload);
    } else{
        return $response->withStatus(400);  
    }
 
 });

$app->get('/search/{name}', function($request, $response,$args){
   $_name = $args['name']; 
   $product = Product::select(['id','name','price'])->where('name', 'like', $_name)->get();
   if($product){
       return $response->withStatus(200)->withJson($product);
   }
   else{
      $payload = ['name'=> $name, "result"=>"The product has been updated successfuly"]; 
      return withJson($payload);
   }
   
});
 

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', /*'http://mysite'*/'*') // * is all the domains
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();
